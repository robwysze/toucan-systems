def read_from_table():
    return int(input("Enter value:\t"))


def first_question():
    print("How many numbers you want enter:")
    quantity = read_from_table()

    my_list = list()
    for i in range(quantity):
        my_list.append(read_from_table())

    for values in my_list:
        print(values)

    for values in my_list:
        tmp = my_list.pop()
        print(str(tmp) + ' was deleted')
        print(my_list)
    print('List is empty')


def second_question():
    print("How many numbers you want enter:")
    quantity = read_from_table()

    my_list = list()
    for i in range(quantity):
        my_list.append(read_from_table())

    for values in my_list:
        print(values)

    for values in my_list:
        tmp = my_list.pop(-1)
        print(str(tmp) + ' was deleted')
        print(my_list)
    print('List is empty')


if __name__ == '__main__':
    first_question()
    second_question()

