#include <iostream>
#include <fstream>

using namespace std;

void first_question();
void second_question();
float enter_tab(float *);

float linear_function(float *, float *, float);

int main() {
	first_question();
	second_question();

	return 0;
}

float linear_function(float *p1, float *p2, float xx) {
	float a = 0.0;
	float b= 0.0;

	a = (p2[1] - p1[1]) / (p2[0] - p1[0]);
	b = a * (-p1[0]) + p1[1];

	return a * xx + b; 
}

void first_question() {
	float p1[2] = {2.0, 5.0};
	float p2[2] = {8.0, 10.0};
	float p3x = 4.0;

	float result = linear_function(p1, p2, p3x);

	cout << result << endl;
}

void second_question() {
	float p1[2] = {0.0}, p2[2] = {0.0};
	int quantity = 0;
	cout << "Enter first point" <<endl;
	enter_tab(p1);
	cout << "Enter second point" <<endl;
	enter_tab(p2);
	cout << "Enter quantity of next x values" << endl;
	cin >> quantity;

	float *x = new float[quantity];
	float *y = new float[quantity];

	cout << "Enter x values:" << endl;
	for(int i = 0; i<quantity; i++) {
		cin >> x[i];
		y[i] = linear_function(p1, p2, x[i]);
	}

	ofstream my_file;
	my_file.open("results.csv");
	my_file << "x, y\n";
	for(int i=0; i<quantity; i++) {
		my_file << x[i] << "," << y[i] << endl;
	}
    my_file.close();


	delete [] y;
	delete [] x;
}

float enter_tab (float *tab) {
	for(int i=0; i<2; i++)
		cin >> tab[i];
}